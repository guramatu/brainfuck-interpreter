#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import argparse
from libs.converter import BrainConverter
from libs.brainfuck import BrainFuck
from libs.brainloller import BrainLoller
from libs.braincopter import BrainCopter


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("convert", choices=["bf2bl", "bf2bc", "bl2bf",
                        "bl2bc", "bc2bf", "bc2bl"], help="Convertion method")
    parser.add_argument("input")
    parser.add_argument("output")

    args = parser.parse_args()


    BrainConverter(args.convert, args.input, args.output)