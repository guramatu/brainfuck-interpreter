#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import libs.converter
import sys


class FakeStdOut:
    def write(self, *args, **kwargs):
        pass
    def flush(self):
        pass


class TestBrainFuck2Other(unittest.TestCase):    
    def setUp(self):
        self.BF = brainx.BrainFuck
        self.out = sys.stdout
        sys.stdout = FakeStdOut()
 
    def tearDown(self):
        sys.stdout = self.out


class TestBrainLoller2Other(unittest.TestCase):    
    def setUp(self):
        self.BF = brainx.BrainFuck
        self.out = sys.stdout
        sys.stdout = FakeStdOut()
 
    def tearDown(self):
        sys.stdout = self.out


class TestBrainCopter2Other(unittest.TestCase):    
    def setUp(self):
        self.BF = brainx.BrainFuck
        self.out = sys.stdout
        sys.stdout = FakeStdOut()
 
    def tearDown(self):
        sys.stdout = self.out


        
if __name__ == '__main__':
    unittest.main()
