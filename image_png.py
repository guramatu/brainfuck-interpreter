#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import zlib
import struct


class PNGWrongHeaderError(Exception):
    pass


class PNGNotImplementedError(Exception):
    pass


class PngReader(object):
    def __init__(self, filepath):
        self.file = open(filepath, mode="rb")
        self.chunks = self.get_chunks()
        (self.width, self.height, self.data) = self.read()
        self.rgb = self._filtering()
        self.file.close()

    def validate_header(self):
        exp_header = struct.pack("8B", 137, 80, 78, 71, 13, 10, 26, 10)
        if self.file.read(8) != exp_header:
            self.file.close()
            raise PNGWrongHeaderError("PNG file has invalid header.")

    def get_chunks(self):
        self.validate_header()
        chunks = []
        while True:
            chunk = {}
            chunk["length"] = int.from_bytes(self.file.read(4), "big")
            chunk["type"] = self.file.read(4)
            chunk["data"] = self.file.read(chunk["length"])
            chunk["checksum"] = self.file.read(4)
            chunks.append(chunk)
            if chunk["type"] == b"IEND":
                break
        return chunks

    def read(self):
        data = b""
        for chunk in self.chunks:
            if chunk["type"] == b"IHDR":
                width = int.from_bytes(chunk["data"][:4], "big")
                height = int.from_bytes(chunk["data"][4:8], "big")
                if chunk["data"][8:13] != struct.pack("5B", 8, 2, 0, 0, 0):
                    self.file.close()
                    raise PNGNotImplementedError()
            elif chunk["type"] == b"IDAT":
                data += chunk["data"]
        return (width, height, zlib.decompress(data))

    def _average_triplet(self, a, b, c):
        return (a[0] + int((b[0] + c[0])/2),
                a[1] + int((b[1] + c[1])/2),
                a[2] + int((b[2] + c[2])/2))

    def _paeth_predictor(self, a, b, c):
        # http://www.w3.org/TR/2003/REC-PNG-20031110/#9Filters
        rgb = tuple()
        for i in range(3):
            p = a[i] + b[i] - c[i]
            pa = abs(p - a[i])
            pb = abs(p - b[i])
            pc = abs(p - c[i])
            if pa <= pb and pa <= pc:
                rgb += (a[i],)
            elif pb <= pc:
                rgb += (b[i],)
            else:
                rgb += (c[i],)
        return rgb

    def _add_rgb(self, a, b):
        return (a[0] + b[0]) % 256, (a[1] + b[1]) % 256, (a[2] + b[2]) % 256

    def _filtering(self):
        # http://www.w3.org/TR/2003/REC-PNG-20031110/#9Filters
        rgb_arr = []
        ptr = 0
        for row in range(self.height):
            filter = self.data[ptr]
            ptr += 1
            row_array = []
            x = (0, 0, 0)  # the byte being filtered
            a = (0, 0, 0)  # the byte immediately before x
            b = (0, 0, 0)  # the byte corresponding to x in the previous line
            c = (0, 0, 0)  # the byte immediately before b
            for col in range(self.width):
                x = (self.data[ptr], self.data[ptr + 1], self.data[ptr + 2])
                ptr += 3
                # none filter
                if filter == 0:
                    a = x
                    row_array += [x]
                # sub filter
                elif filter == 1:
                    a = self._add_rgb(x, a)
                    row_array += [a]
                # up filter
                elif filter == 2:
                    b = rgb_arr[row - 1][col]
                    a = self._add_rgb(x, b)
                    row_array += [a]
                # average filter
                elif filter == 3:
                    b = rgb_arr[row - 1][col]
                    a = row_array[col - 1]
                    x = ((x[0] + (a[0] + b[0]) // 2 + 256) % 256,
                         (x[1] + (a[1] + b[1]) // 2 + 256) % 256,
                         (x[2] + (a[2] + b[2]) // 2 + 256) % 256)
                    row_array += [x]
                # paeth filter
                elif filter == 4:
                    b = rgb_arr[row - 1][col]
                    x = self._add_rgb(x, self._paeth_predictor(a, b, c))
                    a = x
                    c = b
                    row_array += [x]

            rgb_arr += [row_array]
        return rgb_arr


class PngWriter(object):
    def __init__(self, filename, rgb_data):
        self.file = open(filename, mode="wb")
        print(rgb_data)
        self.write(rgb_data)
        self.file.close()

    def write(self, rgb_data):
        header = b"\x89PNG\r\n\x1a\n"

        # IHDR
        ihdr_type = b"IHDR"
        ihdr_data = len(rgb_data).to_bytes(4, 'big') + \
                    int(1).to_bytes(4, 'big') + \
                    b'\x08\x02\x00\x00\x00'
        ihdr_crc = zlib.crc32(ihdr_type + ihdr_data).to_bytes(4, 'big')
        ihdr_size = len(ihdr_data).to_bytes(4, 'big')
        ihdr = ihdr_size + ihdr_type + ihdr_data + ihdr_crc

        # IDAT
        idat_type = b"IDAT"
        idat_data = b"\x00"
        for value in rgb_data:
            idat_data += bytes(value)
        idat_data = zlib.compress(idat_data)
        idat_crc = zlib.crc32(idat_type + idat_data).to_bytes(4, 'big')
        idat_size = len(idat_data).to_bytes(4, 'big')
        idat = idat_size + idat_type + idat_data + idat_crc

        # IEND
        iend = bytes(4) + b"IEND" + zlib.crc32(b'IEND').to_bytes(4, 'big')

        self.file.write(header)
        self.file.write(ihdr)
        self.file.write(idat)
        self.file.write(iend)
