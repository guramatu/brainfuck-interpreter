#!/usr/bin/env python3 
# -*- coding: utf-8 -*-

import sys
from image_png import PngReader
from .brainfuck import BrainFuck


class BrainLoller(object):

    def __init__(self, filename=None, code=None):
        if filename and not code:
            png = PngReader(filename)
            self.decoded = self.decode(png)
            self.program = BrainFuck(self.decoded)
        elif not filename and code:
            self.encoded = self.encode(code)
            self.program = BrainFuck(code)
        else:
            raise NotImplemented()
        self.data = self.program.code

    def decode(self, png):
        decoded = ""
        point = (0, 0)  # row, col
        move = (0, 1)  # right

        while point[0] >= 0 and point[1] >= 0 and \
                point[0] < png.height and point[1] < png.width:
            char, move = self.get_char(png.rgb[point[0]][point[1]], move)
            decoded += char
            point = (point[0] + move[0], point[1] + move[1])

        return decoded

    def encode(self, code):
        rgb_arr = []
        for c in code:
            rgb_arr.append(self.get_colour(c))
        return rgb_arr

    def get_char(self, colour, move):
        output = ""
        if colour == (255, 0, 0):
            output = ">"
        elif colour == (128, 0, 0):
            output = "<"
        elif colour == (0, 255, 0):
            output = "+"
        elif colour == (0, 128, 0):
            output = "-"
        elif colour == (0, 0, 255):
            output = "."
        elif colour == (0, 0, 128):
            output = ","
        elif colour == (255, 255, 0):
            output = "["
        elif colour == (128, 128, 0):
            output = "]"
        elif colour == (0, 255, 255):
            move = self._turn_right(move)
        elif colour == (0, 128, 128):
            move = self._turn_left(move)
        return (output, move)

    def _turn_right(self, move):
        return (move[1], -move[0]) if move[0] else (move[1], move[0])

    def _turn_left(self, move):
        return (move[1], move[0]) if move[0] else (-move[1], move[0])

    def get_colour(self, char):
        if char == ">":
            return (255, 0, 0)
        elif char == "<":
            return (128, 0, 0)
        elif char == "+":
            return (0, 255, 0)
        elif char == "-":
            return (0, 128, 0)
        elif char == ".":
            return (0, 0, 255)
        elif char == ",":
            return (0, 0, 128)
        elif char == "[":
            return (255, 255, 0)
        elif char == "]":
            return (128, 128, 0)
        return (0, 0, 0)

    def render(self):
        print(self.data)
