#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from image_png import PngWriter
from libs.brainfuck import BrainFuck
from libs.brainloller import BrainLoller
from libs.braincopter import BrainCopter


class BrainConverter(object):
    def __init__(self, type, input, output):
        self.type = type
        self.input = input
        self.output = output
        self.execute(type)

    def execute(self, type):
        if self.type.startswith("bf"):
            self._convert_brainfuck()
        elif self.type.startswith("bl"):
            self._convert_brainloller()
        elif self.type.startswith("bc"):
            self._convert_braincopter()

    def _convert_brainfuck(self):
        bf = BrainFuck(self.input)
        with open(self.input, "r") as f:
            input_data = f.read()
        if self.type == "bf2bl":
            bf = BrainLoller(code=input_data)
            self._save_png(bf.encoded)
        elif self.type == "bf2bc":
            bf = BrainCopter(code=input_data)
            self._save_png(bf.encoded)

    def _convert_brainloller(self):
        bl = BrainLoller(self.input)
        if self.type == "bl2bf":
            self._save_text(bl.data)
        elif self.type == "bl2bc":
            bc = BrainCopter(code=bl.data)
            self._save_png(bc.encoded)

    def _convert_braincopter(self):
        bc = BrainCopter(self.input)
        if self.type == "bc2bf":
            self._save_text(bc.data)
        elif self.type == "bc2bl":
            bl = BrainLoller(code=bc.data)
            self._save_png(bl.encoded)

    def _save_text(self, data):
        with open(self.output, "w") as f:
            f.write(data)

    def _save_png(self, data):
        png = PngWriter(self.output, data)
