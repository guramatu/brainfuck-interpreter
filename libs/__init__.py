#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from .brainfuck import BrainFuck
from .brainloller import BrainLoller
from .braincopter import BrainCopter
