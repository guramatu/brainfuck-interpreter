#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys


class BrainFuck(object):
    """
    Brainfuck interpreter
    """
    INC_POINTER = ">"
    DEC_POINTER = "<"
    INC_VALUE = "+"
    DEC_VALUE = "-"
    PRINT_CHAR = "."
    READ_CHAR = ","
    JUMP_FORWARD = "["
    JUMP_BACKWARD = "]"
    INPUT_DELIM = "!"

    def __init__(self, code, memory=b"\x00", memory_pointer=0):
        # save variables
        self.code = code
        self.memory = bytearray(memory)
        self.memory_pointer = memory_pointer
        self.output = ""

        # find input
        index = self.code.find(self.INPUT_DELIM)
        if index == -1:
            self.input = None
        else:
            self.input = list(self.code[index+1:])
            self.code = self.code[:index]

        # run execution
        self._decode(self.code)

    def get_memory(self):
        return self.memory

    @property
    def current_value(self):
        return self.memory[self.memory_pointer]

    @current_value.setter
    def current_value(self, value):
        self.memory[self.memory_pointer] = value

    def _decode(self, code):
        ptr = 0
        while ptr < len(code):
            c = code[ptr]
            if c == self.INC_POINTER:
                self._increment_pointer()
            elif c == self.DEC_POINTER:
                self._decrement_pointer()
            elif c == self.INC_VALUE:
                self._increment_value()
            elif c == self.DEC_VALUE:
                self._decrement_value()
            elif c == self.PRINT_CHAR:
                self._print_character()
            elif c == self.READ_CHAR:
                self._read_character()
            elif c == self.JUMP_FORWARD:
                ptr += self._jump_forward(code[ptr:])
            ptr += 1

    def _increment_pointer(self):
        self.memory_pointer += 1
        if len(self.memory) == self.memory_pointer:
            self.memory += bytearray(1)

    def _decrement_pointer(self):
        self.memory_pointer = max(0, self.memory_pointer - 1)

    def _increment_value(self):
        self.current_value = (self.current_value + 1) % 256

    def _decrement_value(self):
        self.current_value = (self.current_value - 1) % 256

    def _print_character(self):
        sys.stdout.write(chr(self.current_value))
        self.output += chr(self.current_value)

    def _read_character(self):
        if self.input:
            char = self.input.pop(0)
        else:
            char = sys.stdin.read(1)
        self.current_value = ord(char)

    def _jump_forward(self, code):
        left_brackets = 0
        right_brackets = 0
        for i, c in enumerate(code):
            if c == self.JUMP_FORWARD:
                left_brackets += 1
            elif c == self.JUMP_BACKWARD:
                right_brackets += 1
            if left_brackets == right_brackets:
                break
        else:
            raise ValueError("Supplied input is not valid!")
        while self.current_value != 0:
            self._decode(code[1:i])
        return i

    def render(self):
        print(self.output)
