#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from .brainloller import BrainLoller


class BrainCopter(BrainLoller):
    def get_char(self, colour, move):
        output = ""
        command = (-2*colour[0] + 3*colour[1] + colour[2]) % 11
        if command < 8:
            output = "><+-.,[]"[command]
        if command == 8:
            move = self._turn_right(move)
        if command == 9:
            move = self._turn_left(move)
        return (output, move)

    def get_colour(self, char):
        # randomly chosen
        if char == ">":
            return (0, 0, 0)
        elif char == "<":
            return (0, 128, 255)
        elif char == "+":
            return (0, 0, 255)
        elif char == "-":
            return (255, 0, 128)
        elif char == ".":
            return (128, 0, 128)
        elif char == ",":
            return (255, 0 , 0)
        elif char == "[":
            return (0, 255, 0)
        elif char == "]":
            return (0, 0, 128)
        return (0, 128, 0)
