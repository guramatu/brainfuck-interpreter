#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import argparse
from libs.brainfuck import BrainFuck
from libs.brainloller import BrainLoller
from libs.braincopter import BrainCopter


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", help="It is optional for BrainFuck. It is \
        required for BrainLoller and BrainCopter interpreters.")
    parser.add_argument("-f", "--brainfuck", action="store_true",
                        help="Use BrainFuck interpreter")
    parser.add_argument("-l", "--brainloller", action="store_true",
                        help="Use BrainLoller interpreter")
    parser.add_argument("-c", "--braincopter", action="store_true",
                        help="Use BrainCopter interpreter")

    args = parser.parse_args()

    if sum([args.brainfuck, args.brainloller, args.braincopter]) != 1:
        print("\nCHOOSE ONLY ONE INTERPRETER\n")
        parser.print_help()
        sys.exit(1)

    if args.brainfuck:
        if args.input:
            with open(args.input) as f:
                input = f.read()
        else:
            input = sys.stdin.readline()
        BrainFuck(input).render()
    elif args.brainloller and args.input:
        print("BrainFuck:")
        bl = BrainLoller(args.input)
        bl.render()
    elif args.braincopter and args.input:
        print("BrainFuck:")
        bc = BrainCopter(args.input)
        bc.render()
    else:
        print("\nYou did not specify input file!\n".upper())
        parser.print_help()
        sys.exit(1)

    sys.exit(0)
